<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model("login/login_model");
		$this->load->model("produk/produk_model");
		$this->load->model("pembayaran/pembayaran_model");
		$this->load->model("logpembelian/logpembelian_model");
	}

	public function index()
	{
		$this->load->helper('url');
		$produk = $this->produk_model->get_data_by_id("1");
		$product = $this->produk_model->getall_produk();
		$logins = $this->login_model->login_admin("salsabila.i.y13@gmail.com");
		$pembayaran = $this->pembayaran_model->getall_pembayaran();
		$logpembelian = $this->logpembelian->getall_logpembelian();
		$data['login'] = $logins;
		$data['produk'] = $produk;
		$data['product'] = $product;
		$data['pembayaran'] = $pembayaran;
		$data['logpembelian'] = $catatan;

		$this->load->view('home', $data);
	}
}
